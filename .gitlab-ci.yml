variables:
  GIT_STRATEGY: "clone"
  GIT_DEPTH: "10"
  PROJECT_GROUP_NAME: "team-interfaces/cdb"
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository\
    \ -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN\
    \ -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  MAVEN_CLI_OPTS: "--batch-mode\
    \ --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true\
    \ -Dhttp.proxyHost=$PROXY_HOST -Dhttp.proxyPort=$PROXY_PORT -Dhttp.nonProxyHosts=$NO_PROXY\
    \ -Dhttps.proxyHost=$PROXY_HOST -Dhttps.proxyPort=$PROXY_PORT -Dhttps.nonProxyHosts=$NO_PROXY"

cache: &project-cache
  key: "$CI_PROJECT_ID"
  paths:
    - "$CI_PROJECT_DIR/.m2/repository"

.stage-template: &stage-template
  image: maven:3-openjdk-15

stages:
  - clone
  - copy
  - versioning
  - merge
  - transpile
  - update
  - publish

Clone Source Repo:
  <<: *stage-template
  stage: clone
  variables:
    SOURCE_ORIGIN_URL: "https://gitlab-ci-token:$GITLAB_TOKEN@$CI_SERVER_HOST/$PROJECT_GROUP_NAME/$SOURCE_REPO_NAME.git"
  script:
    - git clone $SOURCE_ORIGIN_URL
    - git checkout develop
    - cp -R $SOURCE_REPO_NAME/docs src/main/resources/$DEPLOYMENT_REPO_NAME
    - |
      if [[ `git status --porcelain` ]]; then
        git commit -am "Update $DEPLOYMENT_REPO_NAME docs"
        git push
      fi
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"

Versioning:
  <<: *stage-template
  stage: versioning
  artifacts:
    paths:
      - version
    expire_in: 6 hour
  before_script:
    - export POM_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout $MAVEN_CLI_OPTS | tail -n 1)
  script:
    - echo $POM_VERSION > version
  rules:
    - if: $CI_COMMIT_BRANCH == "develop" && $CI_PIPELINE_SOURCE != "web"

'Versioning [Release]':
  <<: *stage-template
  stage: versioning
  artifacts:
    paths:
      - pom.xml
      - version
    expire_in: 6 hour
  before_script:
    - export POM_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout $MAVEN_CLI_OPTS | tail -n 1)
    - export NEW_VERSION=${POM_VERSION/-SNAPSHOT/-RC}
  script:
    - mvn release:update-versions -DdevelopmentVersion=0.0.1-SNAPSHOT $MAVEN_CLI_OPTS
    - mvn versions:set -DnewVersion=$NEW_VERSION $MAVEN_CLI_OPTS
    - echo $NEW_VERSION > version
  rules:
    - if: $CI_COMMIT_BRANCH == "/^release/.*$/" && $CI_PIPELINE_SOURCE != "web"

'Versioning [Master]':
  <<: *stage-template
  stage: versioning
  artifacts:
    paths:
      - pom.xml
      - version
    expire_in: 6 hour
  before_script:
    - export POM_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout $MAVEN_CLI_OPTS | tail -n 1)
    - export NEW_VERSION=${POM_VERSION/-SNAPSHOT/-RC}
    - export NEW_VERSION=${NEW_VERSION/-RC/}
  script:
    - mvn release:update-versions -DdevelopmentVersion=0.0.1-SNAPSHOT $MAVEN_CLI_OPTS
    - mvn versions:set -DnewVersion=$NEW_VERSION $MAVEN_CLI_OPTS
    - echo $NEW_VERSION > version
  rules:
    - if: $CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE != "web"

Merge:
  <<: *stage-template
  stage: merge
  cache:
    <<: *project-cache
    policy: pull
  script:
    - |
      mvn clean compile org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P merge \
        -Dconfluence.deleteAncestorPage=false \
        $MAVEN_CLI_OPTS
  artifacts:
    when: always
    paths:
      - target
  rules:
    - if: $CI_PIPELINE_SOURCE != "web"

Transpile:
  <<: *stage-template
  stage: transpile
  cache:
    <<: *project-cache
    policy: pull
  script:
    - |
      mvn org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P replacement \
        -Dconfluence.keepIndex=false \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        $MAVEN_CLI_OPTS
  artifacts:
    when: always
    paths:
      - target
  rules:
    - if: $CI_PIPELINE_SOURCE != "web" && $CI_PIPELINE_SOURCE != "web"

Update:
  <<: *stage-template
  stage: update
  cache:
    <<: *project-cache
    policy: pull
  before_script:
    - export VERSION=$(cat version)
  script:
    - |
      mvn org.sahli.asciidoc.confluence.publisher:asciidoc-confluence-publisher-maven-plugin:0.15.1:publish -P publish-docs \
        -Dconfluence.rootConfluenceUrl=$CONFLUENCE_HOST \
        -Dconfluence.spaceKey=$CONFLUENCE_SPACE_KEY \
        -Dconfluence.username=$CONFLUENCE_USERNAME \
        -Dconfluence.password=$CONFLUENCE_PASSWORD \
        -Dconfluence.ancestorId=$CONFLUENCE_ANCESTOR_ID_DEV \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        -Dconfluence.version=$VERSION \
        -Dconfluence.convertOnly=true \
        $MAVEN_CLI_OPTS
    - |
      mvn org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P update-docs \
        -Dconfluence.rootConfluenceUrl=$CONFLUENCE_HOST \
        -Dconfluence.spaceKey=$CONFLUENCE_SPACE_KEY \
        -Dconfluence.password=$CONFLUENCE_PASSWORD \
        -Dconfluence.username=$CONFLUENCE_USERNAME \
        -Dconfluence.ancestorId=$CONFLUENCE_ANCESTOR_ID_DEV \
        -Dconfluence.rootAncestorId=$CONFLUENCE_ROOT_ANCESTOR_ID \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        -Dconfluence.version=$VERSION \
        $MAVEN_CLI_OPTS
  rules:
    - if: $CI_COMMIT_BRANCH == "develop" && $CI_PIPELINE_SOURCE != "web"

'Update [Release]':
  <<: *stage-template
  stage: update
  cache:
    <<: *project-cache
    policy: pull
  before_script:
    - export VERSION=$(cat version)
  script:
    - |
      mvn org.sahli.asciidoc.confluence.publisher:asciidoc-confluence-publisher-maven-plugin:0.15.1:publish -P publish-docs \
        -Dconfluence.rootConfluenceUrl=$CONFLUENCE_HOST \
        -Dconfluence.spaceKey=$CONFLUENCE_SPACE_KEY \
        -Dconfluence.username=$CONFLUENCE_USERNAME \
        -Dconfluence.password=$CONFLUENCE_PASSWORD \
        -Dconfluence.ancestorId=$CONFLUENCE_ANCESTOR_ID_DEV \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        -Dconfluence.version=$VERSION \
        -Dconfluence.convertOnly=true \
        $MAVEN_CLI_OPTS
    - |
      mvn org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P update-docs \
        -Dconfluence.rootConfluenceUrl=$CONFLUENCE_HOST \
        -Dconfluence.spaceKey=$CONFLUENCE_SPACE_KEY \
        -Dconfluence.password=$CONFLUENCE_PASSWORD \
        -Dconfluence.username=$CONFLUENCE_USERNAME \
        -Dconfluence.ancestorId=$CONFLUENCE_ANCESTOR_ID_INT \
        -Dconfluence.rootAncestorId=$CONFLUENCE_ROOT_ANCESTOR_ID \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        -Dconfluence.version=$VERSION \
        $MAVEN_CLI_OPTS
  rules:
    - if: $CI_COMMIT_BRANCH =~ "/^release/.*$/" && $CI_PIPELINE_SOURCE != "web"

'Update [Master]':
  <<: *stage-template
  stage: update
  cache:
    <<: *project-cache
    policy: pull
  before_script:
    - export VERSION=$(cat version)
  script:
    - |
      mvn org.sahli.asciidoc.confluence.publisher:asciidoc-confluence-publisher-maven-plugin:0.15.1:publish -P publish-docs \
        -Dconfluence.rootConfluenceUrl=$CONFLUENCE_HOST \
        -Dconfluence.spaceKey=$CONFLUENCE_SPACE_KEY \
        -Dconfluence.username=$CONFLUENCE_USERNAME \
        -Dconfluence.password=$CONFLUENCE_PASSWORD \
        -Dconfluence.ancestorId=$CONFLUENCE_ANCESTOR_ID_DEV \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        -Dconfluence.version=$VERSION \
        -Dconfluence.convertOnly=true \
        $MAVEN_CLI_OPTS
    - |
      mvn org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P update-docs \
        -Dconfluence.rootConfluenceUrl=$CONFLUENCE_HOST \
        -Dconfluence.spaceKey=$CONFLUENCE_SPACE_KEY \
        -Dconfluence.password=$CONFLUENCE_PASSWORD \
        -Dconfluence.username=$CONFLUENCE_USERNAME \
        -Dconfluence.ancestorId=$CONFLUENCE_ANCESTOR_ID_PROD \
        -Dconfluence.rootAncestorId=$CONFLUENCE_ROOT_ANCESTOR_ID \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        -Dconfluence.version=$VERSION \
        $MAVEN_CLI_OPTS
  rules:
    - if: $CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE != "web"

Publish:
  <<: *stage-template
  stage: publish
  cache:
    <<: *project-cache
    policy: pull
  before_script:
    - export VERSION=$(cat version)
  script:
    - |
      mvn clean compile org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P merge \
        -Dconfluence.deleteAncestorPage=true \
        $MAVEN_CLI_OPTS
    - |
      mvn org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P replacement \
        -Dconfluence.keepIndex=true \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        $MAVEN_CLI_OPTS
    - |
      mvn org.sahli.asciidoc.confluence.publisher:asciidoc-confluence-publisher-maven-plugin:0.15.1:publish -P publish-docs \
        -Dconfluence.rootConfluenceUrl=$CONFLUENCE_HOST \
        -Dconfluence.spaceKey=$CONFLUENCE_SPACE_KEY \
        -Dconfluence.password=$CONFLUENCE_PASSWORD \
        -Dconfluence.username=$CONFLUENCE_USERNAME \
        -Dconfluence.ancestorId=$CONFLUENCE_ANCESTOR_ID_DEV \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        -Dconfluence.version=$VERSION \
        -Dconfluence.convertOnly=false \
        $MAVEN_CLI_OPTS
  rules:
    - if: $CI_COMMIT_BRANCH == "develop" && $CI_PIPELINE_SOURCE != "web"

'Publish [Release]':
  <<: *stage-template
  stage: publish
  cache:
    <<: *project-cache
    policy: pull
  before_script:
    - export VERSION=$(cat version)
  script:
    - |
      mvn clean compile org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P merge \
        -Dconfluence.deleteAncestorPage=true \
        $MAVEN_CLI_OPTS
    - |
      mvn org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P replacement \
        -Dconfluence.keepIndex=true \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        $MAVEN_CLI_OPTS
    - |
      mvn org.sahli.asciidoc.confluence.publisher:asciidoc-confluence-publisher-maven-plugin:0.15.1:publish -P publish-docs \
        -Dconfluence.rootConfluenceUrl=$CONFLUENCE_HOST \
        -Dconfluence.spaceKey=$CONFLUENCE_SPACE_KEY \
        -Dconfluence.password=$CONFLUENCE_PASSWORD \
        -Dconfluence.username=$CONFLUENCE_USERNAME \
        -Dconfluence.ancestorId=$CONFLUENCE_ANCESTOR_ID_INT \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        -Dconfluence.version=$VERSION \
        -Dconfluence.convertOnly=false \
        $MAVEN_CLI_OPTS
  rules:
    - if: $CI_COMMIT_BRANCH =~ "/^release/.*$/" && $CI_PIPELINE_SOURCE != "web"

'Publish [Master]':
  <<: *stage-template
  stage: publish
  cache:
    <<: *project-cache
    policy: pull
  before_script:
    - export VERSION=$(cat version)
  script:
    - |
      mvn clean compile org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P merge \
        -Dconfluence.deleteAncestorPage=true \
        $MAVEN_CLI_OPTS
    - |
      mvn org.codehaus.mojo:exec-maven-plugin:3.0.0:java -P replacement \
        -Dconfluence.keepIndex=true \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        $MAVEN_CLI_OPTS
    - |
      mvn org.sahli.asciidoc.confluence.publisher:asciidoc-confluence-publisher-maven-plugin:0.15.1:publish -P publish-docs \
        -Dconfluence.rootConfluenceUrl=$CONFLUENCE_HOST \
        -Dconfluence.spaceKey=$CONFLUENCE_SPACE_KEY \
        -Dconfluence.password=$CONFLUENCE_PASSWORD \
        -Dconfluence.username=$CONFLUENCE_USERNAME \
        -Dconfluence.ancestorId=$CONFLUENCE_ANCESTOR_ID_PROD \
        -Dconfluence.pageTitlePrefix=$CONFLUENCE_PAGE_TITLE_PREFIX \
        -Dconfluence.version=$VERSION \
        -Dconfluence.convertOnly=false \
        $MAVEN_CLI_OPTS
  rules:
    - if: $CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE != "web"
