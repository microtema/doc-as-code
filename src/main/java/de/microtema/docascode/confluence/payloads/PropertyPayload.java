package de.microtema.docascode.confluence.payloads;

import de.microtema.docascode.confluence.support.RuntimeUse;

public class PropertyPayload {

    private String key;
    private String value;

    @RuntimeUse
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @RuntimeUse
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
