package de.microtema.docascode.confluence.payloads;

import de.microtema.docascode.confluence.support.RuntimeUse;

public class Storage {

    private String value;

    @RuntimeUse
    public String getRepresentation() {
        return "storage";
    }

    @RuntimeUse
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
