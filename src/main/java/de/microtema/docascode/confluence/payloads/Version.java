package de.microtema.docascode.confluence.payloads;

import de.microtema.docascode.confluence.support.RuntimeUse;

public class Version {

    private int number;
    private String message;
    private Boolean minorEdit;

    @RuntimeUse
    public int getNumber() {
        return this.number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @RuntimeUse
    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @RuntimeUse
    public Boolean getMinorEdit() {
        return this.minorEdit;
    }

    public void setMinorEdit(Boolean minorEdit) {
        this.minorEdit = minorEdit;
    }
}
