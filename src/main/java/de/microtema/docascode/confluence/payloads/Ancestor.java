package de.microtema.docascode.confluence.payloads;

import de.microtema.docascode.confluence.support.RuntimeUse;

public class Ancestor {

    private String id;

    @RuntimeUse
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
