package de.microtema.docascode.confluence.payloads;

import de.microtema.docascode.confluence.support.RuntimeUse;

public class Body {

    private Storage storage;

    @RuntimeUse
    public Storage getStorage() {
        return this.storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

}
