package de.microtema.docascode.confluence.payloads;

import de.microtema.docascode.confluence.support.RuntimeUse;

public class Space {

    private String key;

    @RuntimeUse
    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
