package de.microtema.docascode.confluence;

public class ConfluenceAttachment {

    private final String id;
    private final String title;
    private final String relativeDownloadLink;
    private final int version;

    public ConfluenceAttachment(String id, String title, String relativeDownloadLink, int version) {
        this.id = id;
        this.title = title;
        this.relativeDownloadLink = relativeDownloadLink;
        this.version = version;
    }

    public String getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getRelativeDownloadLink() {
        return this.relativeDownloadLink;
    }

    public int getVersion() {
        return this.version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConfluenceAttachment that = (ConfluenceAttachment) o;

        if (this.version != that.version) return false;
        if (!this.id.equals(that.id)) return false;
        //noinspection SimplifiableIfStatement
        if (!this.title.equals(that.title)) return false;
        return this.relativeDownloadLink.equals(that.relativeDownloadLink);

    }

    @Override
    public int hashCode() {
        int result = this.id.hashCode();
        result = 31 * result + this.title.hashCode();
        result = 31 * result + this.relativeDownloadLink.hashCode();
        result = 31 * result + this.version;
        return result;
    }

    @Override
    public String toString() {
        return "ConfluenceAttachment{" +
                "id='" + this.id + '\'' +
                ", title='" + this.title + '\'' +
                ", relativeDownloadLink='" + this.relativeDownloadLink + '\'' +
                ", version=" + this.version +
                '}';
    }

}
