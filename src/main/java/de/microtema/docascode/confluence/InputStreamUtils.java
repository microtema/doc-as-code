package de.microtema.docascode.confluence;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.stream.Collectors;

import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;

public final class InputStreamUtils {

    private InputStreamUtils() {
        throw new UnsupportedOperationException("Utils class cannot be instantiated");
    }

    public static String inputStreamAsString(InputStream is, Charset encoding) {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(is, encoding))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new RuntimeException("Could not convert InputStream to String ", e);
        }
    }

    public static String fileContent(String filePath, Charset encoding) {

        return fileContent(new File(filePath), encoding);
    }

    public static String fileContent(File file, Charset encoding) {
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            return inputStreamAsString(fileInputStream, encoding);
        } catch (IOException e) {
            throw new RuntimeException("Could not read file", e);
        }
    }

    public static boolean notSameHash(String actualHash, String newHash) {
        return actualHash == null || !actualHash.equals(newHash);
    }

    public static String hash(String content) {
        return sha256Hex(content);
    }

    public static String hash(InputStream content) {
        try {
            return sha256Hex(content);
        } catch (IOException e) {
            throw new RuntimeException("Could not compute hash from input stream", e);
        } finally {
            try {
                content.close();
            } catch (IOException ignored) {
            }
        }
    }

    public static FileInputStream fileInputStream(Path filePath) {
        try {
            return new FileInputStream(filePath.toFile());
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Could not find attachment ", e);
        }
    }

}
