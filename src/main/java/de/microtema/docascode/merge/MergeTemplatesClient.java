package de.microtema.docascode.merge;

import java.io.File;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MergeTemplatesClient {

    public static void main(String[] args) {

        MergeTemplates mergeTemplates = new MergeTemplates();

        File targetRootFolder = new File(args[0]);

        boolean deleteAncestorPage = args.length >= 2 && Boolean.parseBoolean(args[1]);

        mergeTemplates.deleteAncestorPage(targetRootFolder, deleteAncestorPage);

        List<File> sourceRootFolders = findFolders(targetRootFolder);

        sourceRootFolders.forEach(it -> mergeTemplates.merge(it, targetRootFolder, deleteAncestorPage));
    }

    private static List<File> findFolders(File folder) {

        File rootFolder = folder.getParentFile();

        File[] files = rootFolder.listFiles();

        if (Objects.isNull(files)) {
            throw new NoSuchElementException("Unable to find files in folder: " + folder.getPath() + "!");
        }

        return Stream.of(files).filter(File::isDirectory).filter(it -> !it.equals(folder)).collect(Collectors.toList());
    }
}
