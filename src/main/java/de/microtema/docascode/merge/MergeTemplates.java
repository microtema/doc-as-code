package de.microtema.docascode.merge;

import de.microtema.docascode.confluence.InputStreamUtils;
import de.microtema.docascode.utils.TemplateUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.microtema.docascode.utils.TemplateUtil.numberPrefix;

public class MergeTemplates {

    private final static Map<String, AtomicInteger> index = new HashMap<>();

    public void merge(File sourceRootFolder, File targetRootFolder, boolean deleteAncestorPage) {

        try {
            mergePages(sourceRootFolder, targetRootFolder, deleteAncestorPage);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private List<File> getAssetFiles(File sourceRootFolder) {

        File[] assetFiles = sourceRootFolder.listFiles((dir, name) -> !name.contains(".adoc"));

        if (Objects.isNull(assetFiles)) {

            return Collections.emptyList();
        }

        return Stream.of(assetFiles).filter(File::isFile).collect(Collectors.toList());
    }

    private void mergePages(File sourceRootFolder, File targetRootFolder, boolean deleteAncestorPage) throws Exception {

        System.out.printf("Merge %s templates to /%s\n", sourceRootFolder.getName(), targetRootFolder.getName());

        List<File> sourceFiles = listFiles(sourceRootFolder);
        List<File> targetFiles = listFiles(targetRootFolder);

        for (File targetFile : targetFiles) {

            if (!targetFile.isFile()) {

                continue;
            }

            if (!targetFile.getName().contains(".adoc")) {

                continue;
            }

            File sourceFile = findFile(targetFile.getName(), sourceFiles);

            if (Objects.isNull(sourceFile)) {

                continue;
            }

            File targetFolder = new File(targetFile.getAbsolutePath().replaceFirst(".adoc", "") + "/" + sourceFile.getParentFile().getName());

            System.out.printf("      Merge ../%s/%s page to ../%s/%s\n", targetFolder.getName(), sourceFile.getName(), targetFolder.getParentFile().getName(), targetFolder.getName());

            targetFolder.mkdirs();

            List<File> assetFiles = getAssetFiles(sourceRootFolder);

            if (deleteAncestorPage) {

                mergeSubPage(sourceFile, targetFolder, targetFolder.getName());

                targetFolder.delete();

                for (File assetFile : assetFiles) {

                    FileUtils.copyFileToDirectory(assetFile, targetFolder.getParentFile());
                }
            } else {

                FileUtils.copyFileToDirectory(sourceFile, targetFolder);

                for (File assetFile : assetFiles) {

                    FileUtils.copyFileToDirectory(assetFile, targetFolder);
                }
            }
        }
    }

    private void mergeSubPage(File sourceFile, File targetFolder, String domainName) throws Exception {

        String prefix = sourceFile.getName().split("_")[0];

        String newPageName = sourceFile.getName().replace(".adoc", "_" + domainName + ".adoc");

        File newPageFile = new File(targetFolder.getParentFile(), newPageName);

        int childIndex = getSubPageIndex(newPageFile.getParentFile().getName());

        System.out.printf("      Merge ../%s/%s page to ../%s/%s\n", targetFolder.getName(), sourceFile.getName(), newPageFile.getParentFile().getName(), newPageFile.getName());

        FileUtils.copyFile(sourceFile, newPageFile);

        renamePageTitle(newPageFile, prefix, childIndex);
    }

    private int getSubPageIndex(String name) {

        AtomicInteger count = index.get(name);

        if (Objects.isNull(count)) {
            count = new AtomicInteger();
            index.put(name, count);
        }

        return count.getAndIncrement();
    }

    private void renamePageTitle(File page, String parentIndex, int childIndex) throws Exception {


        String pageTitle = TemplateUtil.getPageTitle(page);

        String template = InputStreamUtils.fileContent(page, Charset.defaultCharset());

        String newPageTitle = parentIndex + "." + numberPrefix(childIndex + 1) + " " + pageTitle;

        String newTemplate = template.replaceFirst(pageTitle, newPageTitle);

        System.out.printf("      Rename page %s title [%s] to [%s]\n", page.getName(), pageTitle, newPageTitle);

        IOUtils.write(newTemplate, new FileOutputStream(page.getAbsolutePath()), Charset.defaultCharset());
    }

    private static File findFile(String fileName, List<File> targetFiles) {

        return targetFiles.stream()
                .filter(it -> it.getName().equalsIgnoreCase(fileName))
                .findFirst()
                .orElse(null);
    }

    private static List<File> listFiles(File folder) {

        File[] files = folder.listFiles();

        if (Objects.isNull(files)) {
            throw new NoSuchElementException("Unable to find files in folder: " + folder.getPath() + "!");
        }

        return Stream.of(files).filter(File::isFile).collect(Collectors.toList());
    }

    public void deleteAncestorPage(File targetRootFolder, boolean deleteAncestorPage) {

        if (!deleteAncestorPage) {
            return;
        }

        File ancestorPage = new File(targetRootFolder, "00_documentation.adoc");

        if (!ancestorPage.exists()) {
            throw new NoSuchElementException("Unable to find file: " + ancestorPage.getName() + "!");
        }

        if (!ancestorPage.delete()) {
            throw new NoSuchElementException("Unable to delete file: " + ancestorPage.getName() + "!");
        }
    }
}
