package de.microtema.docascode.update;


import de.microtema.docascode.confluence.*;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.microtema.docascode.confluence.InputStreamUtils.fileInputStream;
import static de.microtema.docascode.confluence.InputStreamUtils.notSameHash;
import static de.microtema.docascode.utils.TemplateUtil.getPageTitle;

public class UpdatePage {

    public static void main(String[] args) throws Exception {

        String rootConfluenceUrl = args[0];
        String rootAncestorId = args[1];
        String username = args[2];
        String password = args[3];
        String pageId = args[4];
        String versionMessage = args[5];
        boolean notifyWatchers = Boolean.parseBoolean(args[6]);
        String targetFolderPath = args[7];

        File assetsFolder = getAssetFolder(new File(targetFolderPath, "asciidoc-confluence-publisher/assets"));
        File docFile = new File(new File(new File(targetFolderPath, "classes"), "docs"), "00_documentation.adoc");

        ConfluenceRestClient confluenceRestClient = new ConfluenceRestClient(rootConfluenceUrl, true, false, null, username, password);

        updatePageContent(confluenceRestClient, assetsFolder, docFile, pageId, rootAncestorId, versionMessage, notifyWatchers);
        createAndUpdateAttachments(confluenceRestClient, assetsFolder, pageId, notifyWatchers);
    }

    private static File getAssetFolder(File assetsFolder) {

        File[] assets = assetsFolder.listFiles();

        if (Objects.isNull(assets)) {
            throw new IllegalStateException("Unable to find assets!");
        }

        for (File asset : assets) {

            File[] files = asset.listFiles(it -> it.getName().equalsIgnoreCase("00_documentation.html"));

            if (Objects.isNull(files)) {
                continue;
            }

            if (files.length == 0) {
                continue;
            }

            return asset;
        }

        return null;
    }

    private static void updatePageContent(ConfluenceRestClient confluenceClient, File assetFolder, File docFile, String pageId, String ancestorId, String versionMessage, boolean notifyWatchers) throws Exception {

        String pageTitle = getPageTitle(docFile);
        File htmlFile = new File(assetFolder, "00_documentation.html");
        String pageBody = InputStreamUtils.fileContent(htmlFile, StandardCharsets.UTF_8);

        System.out.printf("Update page %s [%s] size[%s]\n", pageId, pageTitle, pageBody.length());

        ConfluencePage confluencePage = confluenceClient.getPageWithContentAndVersionById(pageId);
        int versionId = confluencePage.getVersion() + 1;

        confluenceClient.updatePage(pageId, ancestorId, pageTitle, pageBody, versionId, versionMessage, notifyWatchers);
    }

    private static void createAndUpdateAttachments(ConfluenceRestClient confluenceClient, File page, String pageId, boolean notifyWatchers) {

        List<File> confluenceAttachments = getConfluenceAttachments(page);

        confluenceAttachments.forEach(it -> addOrUpdateAttachment(confluenceClient, pageId, it.getPath(), it.getName(), notifyWatchers));
    }

    private static void addOrUpdateAttachment(ConfluenceRestClient confluenceClient, String contentId, String attachmentPath, String attachmentFileName, boolean notifyWatchers) {

        Path absoluteAttachmentPath = Paths.get(attachmentPath);
        String newAttachmentHash = InputStreamUtils.hash(fileInputStream(absoluteAttachmentPath));

        try {
            ConfluenceAttachment existingAttachment = confluenceClient.getAttachmentByFileName(contentId, attachmentFileName);
            String attachmentId = existingAttachment.getId();
            String existingAttachmentHash = confluenceClient.getPropertyByKey(contentId, getAttachmentHashKey(attachmentFileName));

            if (notSameHash(existingAttachmentHash, newAttachmentHash)) {
                if (existingAttachmentHash != null) {
                    confluenceClient.deletePropertyByKey(contentId, getAttachmentHashKey(attachmentFileName));
                }
                confluenceClient.updateAttachmentContent(contentId, attachmentId, fileInputStream(absoluteAttachmentPath), notifyWatchers);
                confluenceClient.setPropertyByKey(contentId, getAttachmentHashKey(attachmentFileName), newAttachmentHash);
            }

        } catch (NotFoundException e) {
            confluenceClient.deletePropertyByKey(contentId, getAttachmentHashKey(attachmentFileName));
            confluenceClient.addAttachment(contentId, attachmentFileName, fileInputStream(absoluteAttachmentPath));
            confluenceClient.setPropertyByKey(contentId, getAttachmentHashKey(attachmentFileName), newAttachmentHash);
        }
    }

    private static List<File> getConfluenceAttachments(File page) {

        File[] files = page.listFiles(it -> !it.getName().contains(".adoc"));

        if (Objects.isNull(files)) {
            return Collections.emptyList();
        }

        return Stream.of(files).collect(Collectors.toList());
    }

    private static String getAttachmentHashKey(String attachmentFileName) {
        return attachmentFileName + "-hash";
    }
}
