package de.microtema.docascode.replacement;

import de.microtema.docascode.utils.TemplateUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.microtema.docascode.utils.TemplateUtil.numberPrefix;

public class TemplateReplacement {

    public static void main(String[] args) throws Exception {

        boolean keepIndex = args.length <= 0 || Boolean.parseBoolean(args[0]);
        String pageTitlePrefix = args[1];
        String pageTitleSuffix = args[2];

        File docs = getDocFolder();

        List<File> pages = getPages(docs);

        for (int index = 0; index < pages.size(); index++) {

            File page = pages.get(index);

            String pageName = page.getName();

            String template = getPageContent(page);

            File pageFolder = new File(docs, pageName.replace(".adoc", ""));
            List<String> includeFiles = resolveIncludes(pageFolder, null);

            Map<String, Object> context = new HashMap<>();

            context.put("includes", includeFiles);
            context.put("pageTitlePrefix", pageTitlePrefix);
            context.put("pageTitleSuffix", pageTitleSuffix);

            if (keepIndex) {
                context.put("index", numberPrefix(index + 1));
            }

            String compiledTemplate = TemplateUtil.compileExpression(template, context);

            IOUtils.write(compiledTemplate, new FileOutputStream(page.getAbsolutePath()), Charset.defaultCharset());

            System.out.printf("Transpile page: %s within context: %s\n", pageName, context);
        }
    }



    private static File getDocFolder() {

        URL resource = TemplateReplacement.class.getResource("/docs");

        if (Objects.isNull(resource)) {
            throw new NoSuchElementException("Unable to find /docs folder!");
        }

        return new File(resource.getFile());
    }

    private static List<File> getPages(File rootFile) {

        File[] listFiles = rootFile.listFiles();

        if (Objects.isNull(listFiles)) {
            throw new NoSuchElementException("Unable to find files in /docs!");
        }

        return Stream.of(listFiles)
                .filter(File::isFile)
                .filter(it -> it.getName().contains(".adoc"))
                .filter(it -> StringUtils.isNumeric(it.getName().split("_")[0]))
                .sorted((it, other) -> it.getName().split("_")[0].compareTo(other.getName().split("_")[0]))
                .collect(Collectors.toList());
    }

    private static List<String> resolveIncludes(File pageFolder, String parentFolderName) {

        if (!pageFolder.exists()) {
            return Collections.emptyList();
        }

        File[] files = pageFolder.listFiles();

        if (Objects.isNull(files)) {
            return Collections.emptyList();
        }

        List<String> list = new ArrayList<>();

        for (File fileOrFolder : files) {

            if (fileOrFolder.isDirectory()) {

                String prefixPath = Stream.of(parentFolderName, fileOrFolder.getName()).filter(Objects::nonNull).collect(Collectors.joining("/"));

                list.addAll(resolveIncludes(fileOrFolder, prefixPath));
            } else if (fileOrFolder.getName().contains(".adoc")) {

                if (Objects.nonNull(parentFolderName)) {

                    list.add(parentFolderName + "/" + fileOrFolder.getName());
                } else {

                    list.add(fileOrFolder.getName());
                }
            }
        }

        return list;
    }

    private static String getPageContent(File pageFile) throws Exception {

        if (pageFile.isDirectory()) {
            throw new NoSuchElementException("Page is not a file");
        }

        InputStream inputStream = TemplateReplacement.class.getResourceAsStream("/docs/" + pageFile.getName());

        if (Objects.isNull(inputStream)) {
            throw new NoSuchElementException("Page content is not a file");
        }

        return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
    }
}
