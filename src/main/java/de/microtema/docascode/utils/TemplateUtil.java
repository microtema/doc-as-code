package de.microtema.docascode.utils;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Scanner;

public class TemplateUtil {

    private final static MustacheFactory mustacheFactory = new DefaultMustacheFactory();

    public static String compileExpression(String template, Map<String, Object> context) {

        return compileExpression(template, context, mustacheFactory);
    }

    private static String compileExpression(String template, Map<String, Object> context, MustacheFactory mustacheFactory) {

        Mustache mustache = mustacheFactory.compile(new StringReader(template), template);

        Writer execute = mustache.execute(new StringWriter(), context);

        return execute.toString();
    }

    public static String getPageTitle(File page) throws Exception {

        try (Scanner scanner = new Scanner(Paths.get(page.getPath()))) {

            if (scanner.hasNext()) {
                return scanner.nextLine().replace("=", "").trim();
            }
        }

        throw new IllegalStateException("Unable to find the page title!");
    }

    public static String numberPrefix(int index) {

        if (index < 10) {
            return "0" + index;
        }

        return String.valueOf(index);
    }
}
